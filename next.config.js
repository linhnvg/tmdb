/** @format */

const withPlugins = require('next-compose-plugins');
// const withPWA = require('next-pwa');
const withImages = require('next-images');
// const runtimeCaching = require('next-pwa/cache');

const env = {
  DOMAIN_SERVER: process.env.DOMAIN_SERVER,
  API_KEY: process.env.API_KEY,
  GTM_KEY: process.env.GTM_KEY,
};

// const pwaLoader = withPWA({
//   pwa: {
//     dest: 'public',
//     sourcemap: false,
//     register: true,
//     skipWaiting: true,
//     runtimeCaching,
//     scope: '/',
//     disable: process.env.NODE_ENV === 'development',
//     // swSrc: 'service-worker.js',
//   },
// });

const nextImageConfig = {
  images: {
    domains: ['https://image.tmdb.org/t/p/w500', 'image.tmdb.org'],
    deviceSizes: [320, 375, 414, 540, 640, 717, 750, 768, 828, 1024, 1080, 1200, 1920, 2048, 3840],
  },
};

const nextConfig = {
  webpackDevMiddleware: (config) => {
    // eslint-disable-next-line no-param-reassign
    config.watchOptions = {
      poll: 1000,
      aggregateTimeout: 300,
    };
    return config;
  },
  ...nextImageConfig,
  i18n: {
    locales: ['vn', 'en'],
    defaultLocale: 'vn',
    localeDetection: false,
  },
  env,
};

module.exports = withPlugins([[withImages]], nextConfig);
