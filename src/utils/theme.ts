/** @format */

import { THEME } from '@variables/theme';
import { isServer } from '@utils/server';

const getThemeSystem = () => {
  if (isServer) return THEME.LIGHT;
  // eslint-disable-next-line no-undef
  if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
    return THEME.DARK;
  }
  return THEME.LIGHT;
};

const themeSystem = getThemeSystem();
export default themeSystem;
