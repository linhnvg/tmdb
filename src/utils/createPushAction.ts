/** @format */

export default function createPushAction(type: string, payload: any = {}) {
  return { type, payload, timestamp: Date.now() };
}
