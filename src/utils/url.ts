/**
 * /*
 * https://stackoverflow.com/questions/1053902/how-to-convert-a-title-to-a-url-slug-in-jquery
 *
 * https://gist.github.com/sgmurphy/3095196
 *
 * @format
 */

const url = (text: string) =>
  text
    .toLowerCase()
    .replace(/ /g, '-')
    .replace(/[^\w-]+/g, '');

const createUrl = (text: string, id: number) => {
  if (!text || !id) return '';
  const textTemp = url(text);
  // eslint-disable-next-line consistent-return
  return `/${textTemp}-${id}`;
};

export const getIdBySlug = (slug: string | undefined) => {
  if (!slug) return null;
  const slugTemp = slug.split('-');
  return slugTemp[slugTemp.length - 1];
};

export default createUrl;
