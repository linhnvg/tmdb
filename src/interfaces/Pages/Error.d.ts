/** @format */

// #region Global Imports
import { WithTranslation } from 'next-i18next';
// #endregion Global Imports

declare namespace IErrorPage {
  export interface Props extends WithTranslation {
    statusCode?: number;
  }

  export interface InitialProps {
    namespacesRequired: string[];
  }
}

export { IErrorPage };
