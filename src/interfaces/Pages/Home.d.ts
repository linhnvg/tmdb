/** @format */

// #region Global Imports
import { WithTranslation } from 'next-i18next';
// #endregion Global Imports

declare namespace IHomePage {
  export interface Props extends WithTranslation {
    movies: any;
  }

  export interface InitialProps {
    namespacesRequired: string[];
  }

  export interface StateProps {
    home: {
      version: number;
    };
    image: {
      url: string;
    };
  }

  namespace Actions {
    export interface MapPayload {
      test: string;
    }

    export interface MapResponse {
      test: string;
    }
  }
}

export { IHomePage };
