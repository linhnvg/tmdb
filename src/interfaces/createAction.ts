/** @format */

export interface CreateAction {
  type: string;
  payload: any;
}

// eslint-disable-next-line no-shadow
export enum HTTP_METHOD {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  PATCH = 'PATCH',
  DELETE = 'DELETE ',
}
