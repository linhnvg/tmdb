/** @format */

import rootReducer from '@Reducers';

export namespace Global {
  export type TReducers = ReturnType<typeof rootReducer>;
  export interface GlobalState {
    theme: string;
    language: string;
  }
}
