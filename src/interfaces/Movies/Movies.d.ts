/** @format */

export namespace Movies {
  export interface MoviesState {
    movies: any;
    loading: boolean;
    error: any;
  }
  export interface Movie {
    adult: string;
    backdrop_path: string;
    genre_ids: number[];
    id: number;
    original_language: string;
    original_title: string;
    overview: string;
    popularity: number;
    poster_path: string;
    release_date: string;
    title: string;
    video: boolean;
    vote_average: string;
    vote_count: number;
  }
}
