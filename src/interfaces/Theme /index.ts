/** @format */

// eslint-disable-next-line no-shadow
export enum theme {
  LIGHT = 'light',
  DARK = 'dark',
  SYSTEM = 'system',
}
