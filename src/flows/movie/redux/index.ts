/** @format */

import { MOVIE_ACTION } from '@flows/movie/action';
import { Movies } from '@interfaces/Movies/Movies';

export interface MovieBase {
  data: {
    page: number;
    results: Movies.Movie[];
  } | null;
  error: any;
  movie?: Movies.Movie | null;
}

const INITIAL_STATE: MovieBase = {
  data: null,
  error: null,
  movie: null,
};

export const MoviesReducer = (state = INITIAL_STATE, action: any) => {
  const { payload = {} } = action;
  const { data } = payload;
  switch (action.type) {
    case MOVIE_ACTION.UPDATE_LIST:
      if (state.data && state.data.page !== data.page) {
        const resultTemp = [...state.data.results, ...data.results];
        const stateTemp = Object.assign({}, state, {
          data: action.payload.data || {},
        });
        stateTemp.data.results = resultTemp;
        return { ...stateTemp };
      }
      return Object.assign({}, state, {
        data: data || {},
      });
    case MOVIE_ACTION.UPDATE:
      return {
        ...state,
        data: {
          results: [{ ...data }],
        },
      };
    default:
      return state;
  }
};
