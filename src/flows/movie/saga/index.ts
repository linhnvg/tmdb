/** @format */

import { take, fork, call, put } from 'redux-saga/effects';
import { MOVIE_ACTION_UI, updateMovie, updateListMovie } from '@flows/movie/action';
import { callApi } from '../../../apis';

function* doGetListMovieWorker(result: { payload: any }): any {
  const { payload } = result;
  const response = yield call(callApi, 'movie/popular', {
    language: 'vi-VN',
    page: payload.page || 1,
    region: 'US',
  });
  if (response.status === 200) {
    yield put(updateListMovie(response.data));
  }
  return true;
}

export function* getListMovieWatcher(): any {
  while (true) {
    const result = yield take(MOVIE_ACTION_UI.GET_LIST);
    if (result) {
      yield fork(doGetListMovieWorker, result);
    }
  }
}

function* doGetMovieByIdWorker(result: { payload: any }): any {
  const { payload } = result;
  const response = yield call(callApi, `movie/${payload.id}`, {
    language: 'vi-VN',
    region: 'US',
  });
  if (response.status === 200) {
    yield put(updateMovie(response.data));
  }
  return true;
}

export function* getMovieByIdWatcher(): any {
  while (true) {
    const result = yield take(MOVIE_ACTION_UI.GET_BY_ID);
    if (result) {
      yield fork(doGetMovieByIdWorker, result);
    }
  }
}
