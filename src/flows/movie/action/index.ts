/** @format */
import createPushAction from '@utils/createPushAction';

export const MOVIE_ACTION_UI = {
  GET_LIST: 'MOVIE_ACTION_UI_GET_LIST',
  GET_BY_ID: 'MOVIE_ACTION_UI_GET_BY_ID',
};

export const MOVIE_ACTION = {
  UPDATE_LIST: 'MOVIE_ACTION_UPDATE_LIST',
  UPDATE: 'MOVIE_ACTION_UPDATE',
};

/*
 * Saga
 *  */
export const getListMovieUi = (page?: number) =>
  createPushAction(MOVIE_ACTION_UI.GET_LIST, { page });

export const getMovieUi = (id?: null | string) =>
  createPushAction(MOVIE_ACTION_UI.GET_BY_ID, { id });

/*
 * Redux
 *  */

export const updateListMovie = (data: any) => createPushAction(MOVIE_ACTION.UPDATE_LIST, { data });
export const updateMovie = (data: any) => createPushAction(MOVIE_ACTION.UPDATE, { data });
