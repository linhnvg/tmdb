/** @format */

import { combineReducers } from 'redux';

import { STORE_REDUX_KEY } from '@variables/storeReduxKey';

import { MoviesReducer } from '@flows/movie/redux';

export default combineReducers({
  [STORE_REDUX_KEY.MOVIES]: MoviesReducer,
});
