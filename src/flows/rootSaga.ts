/** @format */

import { all, fork } from 'redux-saga/effects';

import { getListMovieWatcher, getMovieByIdWatcher } from './movie/saga';

function* rootSaga(): any {
  try {
    yield all([fork(getListMovieWatcher), fork(getMovieByIdWatcher)]);
  } catch (error) {
    console.log('ROOT SAGA:::', error);
  }
}

export default rootSaga;
