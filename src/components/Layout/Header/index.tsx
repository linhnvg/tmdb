/**
 * @format
 * @@ Cat @@
 */

import React from 'react';
import classnames from 'classnames';

// @@components
import stylesLayout from '@components/Layout/styles/index.module.scss';

// @@Styles
import CustomLink from '@components/Base/NextLink';
import { PAGE_NAME } from '@variables/page';
import Options from '@components/Layout/Options';
import styles from './styles/index.module.scss';

const Index = () => (
  <header className={classnames(styles.header)}>
    <div className={classnames(styles.content, stylesLayout.content)}>
      <div className={classnames(styles.header_container)}>
        <CustomLink href={PAGE_NAME.HOME}>
          <img width={154} height={20} src="/images/logo.svg" alt="The Movie Database (TMDB)" />
        </CustomLink>
        <ul className={styles.nav}>
          <li>
            <CustomLink href={PAGE_NAME.MOVIES}>
              <span>Movies</span>
            </CustomLink>
          </li>
          <li>
            <CustomLink href={PAGE_NAME.TV_SHOWS}>
              <span>TVShows</span>
            </CustomLink>
          </li>
          <li>
            <CustomLink href={PAGE_NAME.PEOPLE}>
              <span>Peoples</span>
            </CustomLink>
          </li>
        </ul>
      </div>
      <div className={classnames(styles.header__options)}>
        <Options />
      </div>
    </div>
  </header>
);
export default Index;
