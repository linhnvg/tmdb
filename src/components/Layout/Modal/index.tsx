/**
 * @format
 * @@ Cat @@
 */

import React, { FC, memo, useState, useCallback, useEffect } from 'react';
import classnames from 'classnames';
import { GrClose } from 'react-icons/gr';

// Styles
import styles from './styles/index.module.scss';

interface Props {
  isVisible: boolean;
  onCloseModal: () => void;
  children: any;
}

const Index: FC<Props> = ({ isVisible, onCloseModal, children }) => {
  const [isStateVisible, setStateVisible] = useState<boolean>(isVisible);

  useEffect(() => {
    if (!isVisible) {
      setStateVisible(false);
    } else {
      setStateVisible(true);
    }
  }, [isVisible]);

  const handleActiveModal = useCallback(() => {
    onCloseModal();
    setStateVisible(false);
  }, [onCloseModal]);

  return (
    <aside
      id="modal"
      className={classnames(
        styles.modal,
        !isStateVisible
          ? styles['modal-transformHide']
          : classnames(styles['modal-transformOpen'], styles['modal-transformActive'])
      )}
    >
      <div role="presentation" className={styles['modal-wrap']} onClick={handleActiveModal}>
        <GrClose size={20} className={styles.modal__close} />
      </div>
      <div className={styles['modal-content']}>{children}</div>
    </aside>
  );
};

export default memo(Index);
