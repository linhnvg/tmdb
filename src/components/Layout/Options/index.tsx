/**
 * @format
 * @@ Cat @@
 */

import React, { useMemo } from 'react';
import { CgOptions } from 'react-icons/cg';
import { Switch, Dropdown, Menu } from 'antd';

// @@Styles
import styles from './styles/index.module.scss';

const Index = () => {
  const menu = useMemo(
    () => (
      <Menu>
        <Menu.Item>
          <div>
            <span>Language</span>
            <span>:&nbsp;</span>
            <span>Country</span>
          </div>
        </Menu.Item>
        <Menu.Item disabled>
          <div>
            <span>DarkMode</span>
            <span>
              <Switch />
            </span>
          </div>
        </Menu.Item>
      </Menu>
    ),
    []
  );
  return (
    <Dropdown overlay={menu}>
      <div className={styles.options}>
        <span>Options</span>
        <CgOptions width={30} height={30} className={styles.options__icon} />
      </div>
    </Dropdown>
  );
};
export default Index;
