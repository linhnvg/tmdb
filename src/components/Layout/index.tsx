/** @format */

import React, { useContext } from 'react';
import { BackTop } from 'antd';
import { ModalActiveHOC } from '@components/contextHOC';
import { ActiveContext } from '@contexts';
import Header from './Header';
import Modal from './Modal';
import Content from './Modal/Content';

// @@Styles
import styles from './styles/index.module.scss';

const ModalContainer = () => {
  const { isActive, setActive }: any = useContext(ActiveContext);
  return (
    <Modal isVisible={isActive} onCloseModal={setActive}>
      <Content />
    </Modal>
  );
};

const Layout = ({ children }: any) => (
  <ModalActiveHOC>
    <>
      <Header />
      <main className={styles.content}>{children}</main>
    </>
    <ModalContainer />
    <BackTop />
  </ModalActiveHOC>
);

export default Layout;
