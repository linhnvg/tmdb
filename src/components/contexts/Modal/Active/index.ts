/**
 * @format
 * @@ Cat @@
 */

import * as React from 'react';

export const ActiveContext = React.createContext({});
