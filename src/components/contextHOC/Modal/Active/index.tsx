/**
 * @format
 * @@ Cat @@
 */

import React from 'react';

// Contexts
import { ActiveContext } from '@contexts';

const Index = ({ children }: any) => {
  const [isActive, setActive] = React.useState<boolean>(false);

  const setActiveNew = React.useCallback(() => setActive(!isActive), [isActive]);

  const store: { isActive: boolean; setActive: () => void } = React.useMemo(
    () => ({
      isActive,
      setActive: setActiveNew,
    }),
    [setActiveNew, isActive]
  );
  return <ActiveContext.Provider value={store}>{children}</ActiveContext.Provider>;
};

export default React.memo(Index);
fdfdvdfbdf
fdsfdsf
dsadas
fsdfds
asdsf
dfggdgfdggssss
Linh33555435354
Linh343fgdggdfg
fsdffsdfdsfsdfsfsfdfdsfsd
Linh36
