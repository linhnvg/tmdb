/**
 * @format
 * @@ Cat @@
 */

// @@ Imports
import React, { ReactChildren, ReactNode } from 'react';
import classnames from 'clsx';
import Link, { LinkProps } from 'next/link';

// Styles
import styles from './styles/index.module.scss';

type NavLinkProps = React.PropsWithChildren<LinkProps> & {
  ref?: any;
  children: ReactChildren | ReactNode;
  className?: string;
  href: string;
};

const CustomLink: React.FC<NavLinkProps> = React.forwardRef(
  ({ href, children, className, ...rest }, ref) => (
    <Link href={href} ref={ref} {...rest}>
      <a className={classnames(styles.navLink, className)}>{children}</a>
    </Link>
  )
);

export default React.memo(CustomLink);
