/** @format */

import axios from 'axios';
import { HTTP_METHOD } from '@interfaces/createAction';
// import { dataFake1, dataFake2 } from '@flows/movie/saga/data.fake';

export const callApi = async (category: string, params: any) => {
  let paramTemp = `?api_key=${process.env.API_KEY}`;
  // eslint-disable-next-line guard-for-in,no-restricted-syntax
  for (const paramKey in params) {
    paramTemp += `&${paramKey}=${params[paramKey]}`;
  }

  return axios({
    method: HTTP_METHOD.GET,
    url: `${process.env.DOMAIN_SERVER}/${category}${paramTemp}`,
  });

  // return new Promise((resolve) => {
  //   setTimeout(() => {
  //     if (params.page === 2) {
  //       console.log('linhnvg::: ===========>::: category', category);
  //       return resolve({
  //         data: dataFake2,
  //         status: 200,
  //       });
  //     }
  //     return resolve({
  //       data: dataFake1,
  //       status: 200,
  //     });
  //   }, 1000);
  // });
  // return true;
};
