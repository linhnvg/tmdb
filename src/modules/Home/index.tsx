/** @format */

import React, { useCallback } from 'react';
import { Button, Row } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import Head from 'next/head';
import Movie from '@modules/Home/Movie';
import { getListMovieUi } from '@flows/movie/action';
import styles from './home.module.scss';

function Index() {
  const { data } = useSelector((state: any) => state.movies) || [];
  const dispatch = useDispatch();
  const handleLoadMore = useCallback(() => {
    dispatch(getListMovieUi(data.page + 1));
  }, [data.page, dispatch]);
  return (
    <>
      <Head>
        <title>The Movie Database (TMDB)</title>
      </Head>
      <section>
        <h1 className={styles.title}>Popular Movies</h1>
        <Row>
          {data?.results.map((movie: any) => (
            <Movie key={movie.id} movie={movie} />
          ))}
        </Row>
        <div className={styles.load}>
          <Button onClick={handleLoadMore} className={styles.load__more} type="primary">
            Load More
          </Button>
        </div>
      </section>
    </>
  );
}

Index.propTypes = {};

export default Index;
