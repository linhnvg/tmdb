/** @format */

import React, { useContext, useMemo } from 'react';
import { Col } from 'antd';
import { Movies } from '@interfaces/Movies/Movies';

import NextLink from '@components/Base/NextLink';
import createUrl from '@utils/url';
import { ActiveContext } from '@contexts/Modal/Active';
import styles from './index.module.scss';

interface Props {
  movie: Movies.Movie;
}

function Index({ movie }: Props) {
  const active = useContext(ActiveContext);

  const href = useMemo(
    () => createUrl(movie.original_title, movie.id),
    [movie.id, movie.original_title]
  );

  return (
    <Col lg={{ span: 6 }} md={{ span: 8 }} sm={{ span: 12 }} xs={{ span: 24 }}>
      <NextLink href={href} className={styles.movie__link}>
        <div className={styles.movie}>
          <div className={styles.movie__title}>
            <span>{movie.title}</span>
          </div>
          <span className={styles.movie__release}>
            Ngày ra mắt
            {movie.release_date}
          </span>
          {active && <div>ssss</div>}
          <img
            alt={movie.title}
            src={`https://image.tmdb.org/t/p/w500${movie.poster_path}`}
            width="100%"
            height="100%"
          />
        </div>
      </NextLink>
    </Col>
  );
}

Index.propTypes = {};

export default Index;
