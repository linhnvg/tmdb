/** @format */

import React, { useMemo } from 'react';
import { useRouter } from 'next/router';
import { getIdBySlug } from '@utils/url';
import { useSelector } from 'react-redux';
import { STORE_REDUX_KEY } from '@variables/storeReduxKey';
import Image from 'next/image';

function Index() {
  const router = useRouter();
  const { slug } = router?.query;
  // @ts-ignore
  const movieId = useMemo(() => getIdBySlug(slug), [slug]);

  const movie = useSelector((state) => {
    // @ts-ignore
    const movies = state[STORE_REDUX_KEY.MOVIES];
    const { results } = movies?.data || {};
    if (!Array.isArray(results)) return {};
    let movieTemp;
    for (let i = 0; i < results.length; i += 1) {
      if (JSON.stringify(results[i].id) === movieId) {
        movieTemp = results[i];
        break;
      }
    }
    return movieTemp;
  });
  console.log({ movie });

  if (!movieId) return null;

  return (
    <div>
      <h1>{movie?.title}</h1>
      <h2>{movie?.original_title}</h2>
      <h2>{movie?.overview}</h2>
      <div style={{ position: 'relative', height: 1000 }}>
        <Image
          alt={movie.title}
          src={`https://image.tmdb.org/t/p/w500${movie.poster_path}`}
          layout="fill"
        />
      </div>
    </div>
  );
}

Index.propTypes = {};

export default Index;
