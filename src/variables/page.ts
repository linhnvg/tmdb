/** @format */

export const PAGE_NAME = {
  HOME: '/',
  MOVIES: '/movies',
  TV_SHOWS: '/tv',
  PEOPLE: '/peoples',
};
