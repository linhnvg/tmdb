/**
 * "off" or 0 - turn the rule off
 * "warn" or 1 - turn the rule on as a warning (doesn’t affect exit code)
 * "error" or 2 - turn the rule on as an error (exit code is 1 when triggered)
 *
 * @format
 */

const fs = require('fs');
const path = require('path');

const prettierOptions = JSON.parse(fs.readFileSync(path.resolve(__dirname, '.prettierrc'), 'utf8'));

module.exports = {
  // Global ESLint Settings
  // =================================
  root: true,
  extends: [
    'airbnb',
    'airbnb/hooks',
    // Disable rules that conflict with Prettier
    // Prettier must be last to override other configs
    'prettier',
    'airbnb-typescript',
    'prettier/@typescript-eslint',
    'plugin:prettier/recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
  ],
  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  rules: {
    'jsx-a11y/anchor-is-valid': 0,
    'ban-ts-comment': 0,
    '@typescript-eslint/no-explicit-any': 0,
    '@typescript-eslint/ban-ts-ignore': 0,
    '@typescript-eslint/ban-ts-comment': 0,
    '@typescript-eslint/no-var-requires': 0,
    '@typescript-eslint/explicit-function-return-type': 0,
    '@typescript-eslint/camelcase': 0,
    /*
     * Javascript
     * */
    'react/default-props-match-prop-types': 0,
    'react/prop-types': 0,
    'prettier/prettier': ['error', prettierOptions],
    'arrow-body-style': [2, 'as-needed'],
    'arrow-parens': ['error', 'always'],
    'block-scoped-var': 2,
    /**
     * Variables
     */
    'brace-style': [
      2,
      '1tbs',
      {
        allowSingleLine: true,
      },
    ],
    camelcase: 0,
    'class-methods-use-this': 0,
    'comma-dangle': [2, 'always-multiline'],
    'comma-spacing': [
      2,
      {
        before: false,
        after: true,
      },
    ],
    /**
     * Possible errors
     */
    'comma-style': [2, 'last'],
    'consistent-return': 2,
    curly: [2, 'multi-line'],
    'default-case': 2,
    'dot-notation': [
      2,
      {
        allowKeywords: true,
      },
    ],
    'eol-last': 2,
    eqeqeq: 2,
    'func-names': 2,
    'guard-for-in': 2,
    'import/extensions': [0],
    'import/imports-first': 0,
    'import/newline-after-import': 0,
    'import/no-cycle': 0,
    'import/no-dynamic-require': 0,
    'import/no-extraneous-dependencies': 0,
    'import/no-named-as-default': 0,
    'import/no-unresolved': 0,
    'import/no-webpack-loader-syntax': 0,
    'import/prefer-default-export': 0,
    'jsx-a11y/aria-props': 2,
    /**
     * Best practices
     */
    'jsx-a11y/heading-has-content': 0,
    'jsx-a11y/img-redundant-alt': 2,
    'jsx-a11y/label-has-for': 2,
    'jsx-a11y/mouse-events-have-key-events': 2,
    'jsx-a11y/role-has-required-aria-props': 2,
    'jsx-a11y/role-supports-aria-props': 2,
    'jsx-quotes': [2, 'prefer-double'],
    'key-spacing': [
      2,
      {
        beforeColon: false,
        afterColon: true,
      },
    ],
    'max-classes-per-file': 0,
    'max-len': 0,
    'new-cap': [
      0,
      {
        newIsCap: true,
      },
    ],
    'newline-per-chained-call': 0,
    'no-alert': 2,
    'no-caller': 2,
    'no-cond-assign': [2, 'always'],
    'no-confusing-arrow': 0,
    'no-console': 1,
    'no-constant-condition': 2,
    'no-debugger': 1,
    'no-dupe-keys': 2,
    'no-duplicate-case': 2,
    'no-else-return': 2,
    'no-empty': 2,
    'no-eq-null': 2,
    'no-eval': 2,
    'no-ex-assign': 2,
    'no-extend-native': 2,
    'no-extra-bind': 2,
    'no-extra-boolean-cast': 0,
    'no-extra-parens': [2, 'functions'],
    'no-extra-semi': 2,
    'no-fallthrough': 2,
    'no-floating-decimal': 2,
    'no-func-assign': 2,
    'no-implied-eval': 2,
    'no-inner-declarations': 2,
    /**
     * Style
     */
    'no-invalid-regexp': 2,
    'no-irregular-whitespace': 2,
    'no-lone-blocks': 2,
    'no-loop-func': 2,
    'no-multi-spaces': 2,
    'no-multi-str': 2,
    'no-multiple-empty-lines': [
      2,
      {
        max: 2,
      },
    ],
    'no-native-reassign': 2,
    'no-nested-ternary': 0,
    'no-new': 2,
    'no-new-func': 2,
    'no-new-object': 2,
    'no-new-wrappers': 2,
    'no-obj-calls': 2,
    'no-octal': 0,
    'no-octal-escape': 2,
    'no-param-reassign': 2,
    'no-proto': 2,
    'no-redeclare': 2,
    'no-return-assign': 2,
    'no-script-url': 2,
    'no-self-compare': 2,
    'no-sequences': 2,
    'no-shadow': 2,
    'no-shadow-restricted-names': 2,
    'no-spaced-func': 2,
    // "jsx-a11y/images-has-alt": 2,
    'no-sparse-arrays': 2,
    'no-throw-literal': 0,
    'no-trailing-spaces': 2,
    // "jsx-a11y/href-no-hash": 2,
    'no-undef': 2,
    'no-underscore-dangle': 0,
    'no-unreachable': 2,
    'no-unused-vars': [
      2,
      {
        vars: 'local',
        args: 'after-used',
      },
    ],
    'no-use-before-define': 2,
    'no-var': 2,
    'no-with': 2,
    'one-var': [2, 'never'],
    'padded-blocks': [2, 'never'],
    'prefer-const': 2,
    'prefer-template': 2,
    quotes: [2, 'single', 'avoid-escape'],
    radix: 2,
    'react/display-name': 0,
    'react/forbid-prop-types': 0,
    'react/jsx-boolean-value': 0,
    'react/jsx-closing-bracket-location': 0,
    'react/jsx-curly-spacing': 2,
    'react/jsx-filename-extension': 0,
    'react/jsx-first-prop-new-line': [2, 'multiline'],
    'react/jsx-no-bind': 2,
    'react/jsx-no-target-blank': 0,
    'react/jsx-no-undef': 2,
    'react/jsx-pascal-case': 2,
    'react/jsx-sort-prop-types': 0,
    'react/jsx-sort-props': 0,
    'react/jsx-space-before-closing': 0,
    'react/jsx-uses-react': 2,
    'react/jsx-uses-vars': 2,
    'react/jsx-wrap-multilines': 0,
    'react/no-array-index-key': 2,
    'react/no-deprecated': 0,
    'react/no-did-mount-set-state': 2,
    'react/no-did-update-set-state': 2,
    'react/no-find-dom-node': 0,
    'react/no-multi-comp': 0,
    'react/jsx-props-no-spreading': 1,
    'react/no-string-refs': 2,
    'react/no-unknown-property': 2,
    'react/prefer-es6-class': 2,
    'react/prefer-stateless-function': 2,
    'react/react-in-jsx-scope': 2,
    'react/require-default-props': 0,
    'react/require-extension': 0,
    'react/require-render-return': 2,
    'react/self-closing-comp': 2,
    'react/sort-comp': 2,
    'redux-saga/no-unhandled-errors': 0,
    'redux-saga/no-yield-in-race': 0,
    'redux-saga/yield-effects': 0,
    'require-yield': 0,
    semi: [2, 'always'],
    'semi-spacing': [
      2,
      {
        before: false,
        after: true,
      },
    ],
    'space-before-blocks': 2,
    'space-before-function-paren': 0, // Tắt đi để prettier quản lý
    'space-infix-ops': 2,
    'spaced-comment': [
      2,
      'always',
      {
        exceptions: ['-', '+'],
        markers: ['=', '!'],
        // space here to support sprockets directives
      },
    ],
    'use-isnan': 2,
    'vars-on-top': 2,
    yoda: 2,
    'global-require': 1,
  },
  settings: {
    'import/parsers': {
      '@typescript-eslint/parser': ['.ts', '.tsx'],
    },
    'import/resolver': {
      typescript: {},
    },
  },
  plugins: ['import', 'prettier', 'react', 'react-hooks', '@typescript-eslint'],
};
