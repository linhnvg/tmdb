/** @format */

import * as React from 'react';
// @@ Components
import Home from '@modules/Home/index';

// Utils
import wrapper from '@redux/store';
import { END } from 'redux-saga';
import { getListMovieUi } from '@flows/movie/action';
import { STORE_REDUX_KEY } from '@variables/storeReduxKey';
import { isEmpty } from 'lodash';

const Index = (props: any) => <Home {...props} />;

export const getStaticProps = wrapper.getStaticProps((store: any) => async () => {
  if (isEmpty(store.getState()[STORE_REDUX_KEY.MOVIES].data)) {
    store.dispatch(getListMovieUi());
    store.dispatch(END);
  }
  await store.sagaTask.toPromise();
});

export default Index;
