/** @format */

import * as React from 'react';

// @@ Components
import Thread from '@modules/Thread';
import wrapper from '@redux/store';
// import createUrl, { getIdBySlug } from '@utils/url';
import { getIdBySlug } from '@utils/url';
import { getMovieUi } from '@flows/movie/action';
import { END } from 'redux-saga';

const Index = (props: any) => <Thread {...props} />;

// This function gets called at build time
// export async function getStaticPaths() {
//   // eslint-disable-next-line no-undef
//   const res = await fetch(
//     `${process.env.DOMAIN_SERVER}/movie/popular?api_key=${process.env.API_KEY}&language=vi-VN&page=1'`
//   );
//   const response = await res.json();
//   // Get the paths we want to pre-render based on posts
//   const movies = JSON.parse(JSON.stringify(response));
//   const paths = movies.results.map((movie: any) => ({
//     params: { slug: createUrl(movie.original_title, movie.id) },
//   }));
//   return {
//     paths,
//     fallback: false,
//   };
// }
//
// export const getStaticProps = wrapper.getStaticProps((store: any) => async ({ params }: any) => {
//   const { slug } = params;
//   const id = getIdBySlug(slug);
//   store.dispatch(getMovieUi(id));
//   store.dispatch(END);
//   await store.sagaTask.toPromise();
// });

// export const getServerSideProps = wrapper.getServerSideProps(
//   (store: any) =>
//     async ({ params }: any) => {
//       const { slug } = params;
//       const id = getIdBySlug(slug);
//       store.dispatch(getMovieUi(id));
//       store.dispatch(END);
//       await store.sagaTask.toPromise();
//     }
// );

/*
 * fallback: true
 * internet: required*
 * ==> Sinh ra file [slug.html] => là file rỗng hay null
 * ==> Khi mà khơi chạy lần đầu tiên thì sẽ tạo ra những file html khác 8 file
 * ==> Khi mà cuộn xuống trang chủ thì tự động tạo thêm các file html khác
 *
 *  */

/*
 * fallback: false
 * internet: required*
 * ==> Không sinh ra Sinh ra file [slug.html]
 * ==> Sinh ra những file đã được return ở paths
 * ==> Nếu không tồn tại return 404
 *
 *  */

/*
 * fallback: blocking
 * internet: required*
 * ==> Không sinh ra Sinh ra file [slug.html]
 * ==> Khi mà khơi chạy lần đầu tiên thì sẽ tạo ra những file html khác 6 file
 * ==> Khi cuộn xuống trang chủ sẽ tự động build html của bài khac
 *
 *  */
export async function getStaticPaths() {
  return {
    paths: [],
    fallback: true,
  };
}

export const getStaticProps = wrapper.getStaticProps((store: any) => async ({ params }: any) => {
  const { slug } = params;
  const id = getIdBySlug(slug);
  store.dispatch(getMovieUi(id));
  store.dispatch(END);
  await store.sagaTask.toPromise();
});

export default Index;
