/** @format */

import React from 'react';
import { Layout } from 'antd';
import App, { AppContext, AppInitialProps } from 'next/app';
import { ThemeProvider } from 'next-themes';
import TagManager from 'react-gtm-module';

// @@Previous CSS
import 'antd/dist/antd.css';
import 'public/styles/global.scss';
import 'public/styles/styles.scss';

// @@ Components
import LayoutChild from '@components/Layout';

// @@ Default
import wrapper from '@redux/store';

const { Content } = Layout;

class WebApp extends App<AppInitialProps> {
  public static getInitialProps = wrapper.getInitialAppProps(
    (store: any) =>
      async ({ Component, ctx }: AppContext) => {
        const pagePropsTemp = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};
        return {
          pageProps: Object.assign(pagePropsTemp, { store }),
        };
      }
  );

  componentDidMount() {
    TagManager.initialize({ gtmId: process.env.GTM_KEY as 'GTM-000000' });
  }

  render() {
    const { Component, pageProps } = this.props;
    return (
      <ThemeProvider>
        <LayoutChild>
          <Content>
            <Component {...pageProps} />
          </Content>
        </LayoutChild>
      </ThemeProvider>
    );
  }
}

export default wrapper.withRedux(WebApp);
